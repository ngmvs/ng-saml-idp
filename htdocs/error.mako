<%inherit file="root.mako"/>

<div class="centered centerbox">
<h2>Error! ${error_reason}.</h2>
<h3>${error_action}</h3>
</div>