<%inherit file="root.mako"/>

<div class="centered centerbox">
<h2>Access the MVS via Newgrounds Passport!</h2>

<div id="loginMsg">
	<h3><a id="loginLink" href="${passport_url}" target="_blank">Click here to log in!</a></h3>
	<noscript>
		<h3>After you've logged in to Newgrounds Passport, <button class="linkbutton" type="submit" form="finalForm">click here</button>.</h3>
	</noscript>
	<p>This will create a Matrix account for you on <strong>ngmvs.one</strong>, using information from your Newgrounds account.<br>(If you already have an account, this will log you back in.)</p>
	<p>For more information, read the <a href="//localhost:8009/_matrix/consent" target="_blank" rel="noopener">MVS User Agreement</a>.</p>
</div>
<div class="hidden" id="checkMsg">
	<h3>Checking...</h3>
	<p>If this page doesn't automatically update after you've logged in to Newgrounds Passport, <button class="linkbutton" type="submit" form="finalForm">click here</button> to advance manually.</p>
</div>
<div class="hidden" id="failureMsg"><h3>Something went wrong! Refresh and try again...</h3></div>

</div>

<form method="post" id="finalForm" action="${action}">
	<input type="hidden" name="sessionID" value="${session_id}">
	<input type="hidden" name="id">
	<input type="hidden" name="name">
	<input type="hidden" name="icon">
	<input type="hidden" name="supporter">

	<input type="hidden" name="key" value="${key}"/>
	<input type="hidden" name="authn_reference" value="${authn_reference}"/>
	<input type="hidden" name="redirect_uri" value="${redirect_uri}"/>
</form>
<script>
const __SESSION_ID = "${session_id}"
</script>
<script src=/static/js/login.js></script>