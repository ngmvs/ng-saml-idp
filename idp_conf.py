#!/usr/bin/env python
# Copyright 2021 Xavier Johnson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os.path

import yaml

#import saml2.xmldsig as ds
from saml2 import BINDING_HTTP_REDIRECT
from saml2 import BINDING_HTTP_ARTIFACT
from saml2 import BINDING_HTTP_POST
from saml2.saml import NAMEID_FORMAT_TRANSIENT
from saml2.saml import NAMEID_FORMAT_PERSISTENT

try:
    from saml2.sigver import get_xmlsec_binary
except ImportError:
    get_xmlsec_binary = None

if get_xmlsec_binary:
    xmlsec_path = get_xmlsec_binary(["/opt/local/bin"])
else:
    xmlsec_path = '/usr/bin/xmlsec1'

BASEDIR = os.path.abspath(os.path.dirname(__file__))


def full_path(local_file):
    return os.path.join(BASEDIR, local_file)

with open("config.yaml") as cfg_file:
    config = yaml.safe_load(cfg_file)

HOST = config["url"]["host"]
PORT = config["url"]["port"]

HTTPS = config["url"]["tls"]

if HTTPS:
    BASE = "https://%s:%s" % (HOST, PORT)
else:
    BASE = "http://%s:%s" % (HOST, PORT)

# HTTPS cert information
SERVER_CERT = "pki/mycert.pem"
SERVER_KEY = "pki/mykey.pem"
CERT_CHAIN = ""
SIGN_ALG = None
DIGEST_ALG = None
#SIGN_ALG = ds.SIG_RSA_SHA512
#DIGEST_ALG = ds.DIGEST_SHA512

NGIO_URL = "https://www.newgrounds.io/gateway_v3.php/"
APP_ID = config["ng"]["app_id"]


CONFIG = {
    "entityid": "%s/idp.xml" % BASE,
    "name": "Newgrounds Passport SAML Identity Provider",
    "description": "Newgrounds Passport SAML Identity Provider",
    "valid_for": 168,
    "service": {
        "idp": {
            "endpoints": {
                "single_sign_on_service": [
                    ("%s/sso/redirect" % BASE, BINDING_HTTP_REDIRECT),
                    ("%s/sso/post" % BASE, BINDING_HTTP_POST),
                    ("%s/sso/art" % BASE, BINDING_HTTP_ARTIFACT),
                ],
            },
            "signing_algorithm": SIGN_ALG,
            "digest_algorithm": DIGEST_ALG,
            "sign_response": True,
            "sign_assertion": True,
            "subject_data": "./idp.subject",
            "name_id_format": [NAMEID_FORMAT_TRANSIENT,
                               NAMEID_FORMAT_PERSISTENT]
        },
    },
    "debug": 1,
    "key_file": full_path(SERVER_KEY),
    "cert_file": full_path(SERVER_CERT),
    "metadata": {
        "local": [full_path("sp.xml")],
        "remote": config["saml"]["remote"]
    },
    "organization": {
        "display_name": "Xavier Johnson",
        "name": "mr-johnson22",
        "url": "https://mr-johnson22.newgrounds.com",
    },
    "contact_person": [
        {
            "contact_type": "technical",
            "given_name": "Xavier",
            "sur_name": "Johnson",
            "email_address": "webmaster@ngmvs.one"
        },
    ],
    # This database holds the map between a subject's local identifier and
    # the identifier returned to a SP
    "xmlsec_binary": xmlsec_path,
    #"attribute_map_dir": "attribute_maps",
    "logging": {
        "version": 1,
        "formatters": {
            "simple": {
                "format": "[%(asctime)s] [%(levelname)s] [%(name)s.%(funcName)s] %(message)s",
            },
        },
        "handlers": {
            "stderr": {
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stderr",
                "level": "DEBUG",
                "formatter": "simple",
            },
        },
        "loggers": {
            "saml2": {
                "level": "DEBUG"
            },
        },
        "root": {
            "level": "DEBUG",
            "handlers": [
                "stderr",
            ],
        },
    },
}
