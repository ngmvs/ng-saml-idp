// @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
function checkSession() {
	const xhr = new XMLHttpRequest()
	xhr.open("POST", "check_session")
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
	xhr.onreadystatechange = function() {
		if (this.readyState === XMLHttpRequest.DONE) {
			if (this.status === 200) {
				let output
				if (!this.reponseType || this.responseType == "text") {
					output = JSON.parse(this.response)
				} else if (this.responseType == "json") {
					output = this.response
				}
				submitForm(output)
			} else if (this.status === 400 || this.status == 401) {
				setTimeout(checkSession, 3000)
			} else {
				showFailure()
			}
		}
	}
	xhr.send(`session_id=${__SESSION_ID}`)
}

function submitForm(user) {
	document.querySelector("input[name=id]").setAttribute("value", user.id)
	document.querySelector("input[name=name]").setAttribute("value", user.name)
	document.querySelector("input[name=icon]").setAttribute("value", user.icons.large)
	document.querySelector("input[name=supporter]").setAttribute("value", user.supporter)
	document.querySelector("#finalForm").submit()
}

function showFailure() {
	document.getElementById("loginMsg").classList.add("hidden")
	document.getElementById("checkMsg").classList.add("hidden")
	document.getElementById("failureMsg").classList.remove("hidden")
}


document.getElementById("loginLink").onclick = function() {
	document.getElementById("loginMsg").classList.add("hidden")
	document.getElementById("checkMsg").classList.remove("hidden")
	checkSession()
}
// @license-end