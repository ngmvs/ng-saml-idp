#!/usr/bin/env python
# Copyright 2021 Xavier Johnson
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import argparse
import base64
import mimetypes
import logging
import os
import re
import requests
from requests import ConnectionError, HTTPError, Timeout
import time
from hashlib import sha1

from mako.lookup import TemplateLookup

import six
from six.moves.http_cookies import SimpleCookie
from six.moves.urllib.parse import parse_qs

import saml2.xmldsig as ds
from saml2 import BINDING_HTTP_ARTIFACT
from saml2 import BINDING_HTTP_POST
from saml2 import BINDING_HTTP_REDIRECT
from saml2 import BINDING_SOAP
from saml2 import server
from saml2 import time_util
from saml2.authn_context import AuthnBroker
from saml2.authn_context import TIMESYNCTOKEN
from saml2.authn_context import UNSPECIFIED
from saml2.authn_context import authn_context_class_ref
from saml2.httputil import BadGateway
from saml2.httputil import BadRequest
from saml2.httputil import NotFound
from saml2.httputil import Redirect
from saml2.httputil import Response
from saml2.httputil import ServiceError
from saml2.httputil import Unauthorized
from saml2.httputil import get_post
from saml2.httputil import geturl
from saml2.metadata import create_metadata_string
from saml2.s_utils import UnknownPrincipal
from saml2.s_utils import UnsupportedBinding
from saml2.s_utils import exception_trace
from saml2.sigver import encrypt_cert_from_item
from saml2.sigver import verify_redirect_signature

import idp_conf
import json

try:
    from cheroot.wsgi import Server as WSGIServer
    from cheroot.ssl.builtin import BuiltinSSLAdapter
except ImportError:
    from cherrypy.wsgiserver import CherryPyWSGIServer as WSGIServer
    from cherrypy.wsgiserver.ssl_builtin import BuiltinSSLAdapter


logger = logging.getLogger("saml2.idp")
logger.setLevel(logging.DEBUG)


class Cache(object):
    def __init__(self):
        self.user2udata = {}


def _expiration(timeout, tformat="%a, %d-%b-%Y %H:%M:%S GMT"):
    """

    :param timeout:
    :param tformat:
    :return:
    """
    if timeout == "now":
        return time_util.instant(tformat)
    elif timeout == "dawn":
        return time.strftime(tformat, time.gmtime(0))
    else:
        # validity time should match lifetime of assertions
        return time_util.in_a_while(minutes=timeout, format=tformat)


# -----------------------------------------------------------------------------


def dict2list_of_tuples(d):
    return [(k, v) for k, v in d.items()]


# -----------------------------------------------------------------------------


class Service(object):
    def __init__(self, environ, start_response, user=None):
        self.environ = environ
        logger.debug("ENVIRON: %s", environ)
        self.start_response = start_response
        self.user = user

    def unpack_redirect(self):
        if "QUERY_STRING" in self.environ:
            _qs = self.environ["QUERY_STRING"]
            return dict([(k, v[0]) for k, v in parse_qs(_qs).items()])
        else:
            return None

    def unpack_post(self):
        post_data = get_post(self.environ)
        _dict = parse_qs(
            post_data
            if isinstance(post_data, str)
            else post_data.decode('utf-8')
        )
        logger.debug("unpack_post:: %s", _dict)
        try:
            return dict([(k, v[0]) for k, v in _dict.items()])
        except Exception:
            return None

    def unpack_soap(self):
        try:
            query = get_post(self.environ)
            return {"SAMLRequest": query, "RelayState": ""}
        except Exception:
            return None

    def unpack_either(self):
        if self.environ["REQUEST_METHOD"] == "GET":
            _dict = self.unpack_redirect()
        elif self.environ["REQUEST_METHOD"] == "POST":
            _dict = self.unpack_post()
        else:
            _dict = None
        logger.debug("_dict: %s", _dict)
        return _dict

    def operation(self, saml_msg, binding):
        logger.debug("_operation: %s", saml_msg)
        if not (saml_msg and "SAMLRequest" in saml_msg):
            resp = BadRequest("Error parsing request or no request")
            return resp(self.environ, self.start_response)
        else:
            # saml_msg may also contain Signature and SigAlg
            if "Signature" in saml_msg:
                try:
                    kwargs = {
                        "signature": saml_msg["Signature"],
                        "sigalg": saml_msg["SigAlg"],
                    }
                except KeyError:
                    resp = BadRequest("Signature Algorithm specification is missing")
                    return resp(self.environ, self.start_response)
            else:
                kwargs = {}

            try:
                kwargs["encrypt_cert"] = encrypt_cert_from_item(
                    saml_msg["req_info"].message
                )
            except KeyError:
                pass

            try:
                kwargs["relay_state"] = saml_msg["RelayState"]
            except KeyError:
                pass

            return self.do(saml_msg["SAMLRequest"], binding, **kwargs)

    def artifact_operation(self, saml_msg):
        if not saml_msg:
            resp = BadRequest("Missing query")
            return resp(self.environ, self.start_response)
        else:
            # exchange artifact for request
            request = IDP.artifact2message(saml_msg["SAMLart"], "spsso")
            try:
                return self.do(request, BINDING_HTTP_ARTIFACT, saml_msg["RelayState"])
            except KeyError:
                return self.do(request, BINDING_HTTP_ARTIFACT)

    def response(self, binding, http_args):
        resp = None
        if binding == BINDING_HTTP_ARTIFACT:
            resp = Redirect()
        elif http_args["data"]:
            resp = Response(http_args["data"], headers=http_args["headers"])
        else:
            for header in http_args["headers"]:
                if header[0] == "Location":
                    resp = Redirect(header[1])

        if not resp:
            resp = ServiceError("Don't know how to return response")

        return resp(self.environ, self.start_response)

    def do(self, query, binding, relay_state="", encrypt_cert=None):
        pass

    def redirect(self):
        """ Expects a HTTP-redirect request """

        _dict = self.unpack_redirect()
        return self.operation(_dict, BINDING_HTTP_REDIRECT)

    def post(self):
        """ Expects a HTTP-POST request """

        _dict = self.unpack_post()
        return self.operation(_dict, BINDING_HTTP_POST)

    def artifact(self):
        # Can be either by HTTP_Redirect or HTTP_POST
        _dict = self.unpack_either()
        return self.artifact_operation(_dict)

    def soap(self):
        """
        Single log out using HTTP_SOAP binding
        """
        logger.debug("- SOAP -")
        _dict = self.unpack_soap()
        logger.debug("_dict: %s", _dict)
        return self.operation(_dict, BINDING_SOAP)

    def uri(self):
        _dict = self.unpack_either()
        return self.operation(_dict, BINDING_SOAP)

    def not_authn(self, key, requested_authn_context):
        ruri = geturl(self.environ, query=False)

        kwargs = dict(authn_context=requested_authn_context, key=key, redirect_uri=ruri)
        # Clear cookie, if it already exists
        kaka = delete_cookie(self.environ, "idpauthn")
        if kaka:
            kwargs["headers"] = [kaka]
        return do_authentication(self.environ, self.start_response, **kwargs)


# -----------------------------------------------------------------------------


REPOZE_ID_EQUIVALENT = "uid"
FORM_SPEC = """<form name="myform" method="post" action="%s">
   <input type="hidden" name="SAMLResponse" value="%s" />
   <input type="hidden" name="RelayState" value="%s" />
</form>"""


# -----------------------------------------------------------------------------
# === Single log in ====
# -----------------------------------------------------------------------------


class AuthenticationNeeded(Exception):
    def __init__(self, authn_context=None, *args, **kwargs):
        Exception.__init__(*args, **kwargs)
        self.authn_context = authn_context


class SSO(Service):
    def __init__(self, environ, start_response, user=None):
        Service.__init__(self, environ, start_response, user)
        self.binding = ""
        self.response_bindings = None
        self.resp_args = {}
        self.binding_out = None
        self.destination = None
        self.req_info = None
        self.op_type = ""

    def verify_request(self, query, binding):
        """
        :param query: The SAML query, transport encoded
        :param binding: Which binding the query came in over
        """
        resp_args = {}
        if not query:
            logger.info("Missing QUERY")
            resp = Unauthorized("Unknown user")
            return resp_args, resp(self.environ, self.start_response)

        if not self.req_info:
            self.req_info = IDP.parse_authn_request(query, binding)

        logger.info("parsed OK")
        _authn_req = self.req_info.message
        logger.debug("%s", _authn_req)

        try:
            self.binding_out, self.destination = IDP.pick_binding(
                "assertion_consumer_service",
                bindings=self.response_bindings,
                entity_id=_authn_req.issuer.text,
                request=_authn_req,
            )
        except Exception as err:
            logger.error("Couldn't find receiver endpoint: %s", err)
            raise

        logger.debug("Binding: %s, destination: %s", self.binding_out, self.destination)

        resp_args = {}
        try:
            resp_args = IDP.response_args(_authn_req)
            _resp = None
        except UnknownPrincipal as excp:
            _resp = IDP.create_error_response(_authn_req.id, self.destination, excp)
        except UnsupportedBinding as excp:
            _resp = IDP.create_error_response(_authn_req.id, self.destination, excp)

        return resp_args, _resp

    def do(self, query, binding_in, relay_state="", encrypt_cert=None, **kwargs):
        """

        :param query: The request
        :param binding_in: Which binding was used when receiving the query
        :param relay_state: The relay state provided by the SP
        :param encrypt_cert: Cert to use for encryption
        :return: A response
        """
        try:
            resp_args, _resp = self.verify_request(query, binding_in)
        except UnknownPrincipal as excp:
            logger.error("UnknownPrincipal: %s", excp)
            resp = ServiceError("UnknownPrincipal: %s" % (excp,))
            return resp(self.environ, self.start_response)
        except UnsupportedBinding as excp:
            logger.error("UnsupportedBinding: %s", excp)
            resp = ServiceError("UnsupportedBinding: %s" % (excp,))
            return resp(self.environ, self.start_response)

        if not _resp:
            # TODO Must save the cache to disk for this to work!
            try:
                identity = IDP.cache.user2udata[self.user].copy()
                logger.info("Identity: %s", identity)
            except KeyError:
                logger.warning("No identity found for %s - using fake values to exit safely", self.user)
                identity = {
                    "uid": self.user,
                    "displayName": "",
                    "member": "user",
                    "jpegPhoto": "",
                }

            if REPOZE_ID_EQUIVALENT:
                identity[REPOZE_ID_EQUIVALENT] = self.user
            try:
                try:
                    metod = self.environ["idp.authn"]
                except KeyError:
                    pass
                else:
                    resp_args["authn"] = metod

                _resp = IDP.create_authn_response(
                    identity,
                    userid=self.user,
                    encrypt_cert_assertion=encrypt_cert,
                    **resp_args
                )
            except Exception as excp:
                logging.error(exception_trace(excp))
                resp = ServiceError("Exception: %s" % (excp,))
                return resp(self.environ, self.start_response)

        logger.info("AuthNResponse: %s", _resp)
        kwargs = {}

        http_args = IDP.apply_binding(
            self.binding_out,
            "%s" % _resp,
            self.destination,
            relay_state,
            response=True,
            **kwargs
        )

        logger.debug("HTTPargs: %s", http_args)
        return self.response(self.binding_out, http_args)

    @staticmethod
    def _store_request(saml_msg):
        logger.debug("_store_request: %s", saml_msg)
        key = sha1(saml_msg["SAMLRequest"].encode()).hexdigest()
        # store the AuthnRequest
        IDP.ticket[key] = saml_msg
        return key

    def redirect(self):
        """ This is the HTTP-redirect endpoint """

        logger.info("--- In SSO Redirect ---")
        saml_msg = self.unpack_redirect()

        try:
            _key = saml_msg["key"]
            saml_msg = IDP.ticket[_key]
            self.req_info = saml_msg["req_info"]
            del IDP.ticket[_key]
        except KeyError:
            try:
                self.req_info = IDP.parse_authn_request(
                    saml_msg["SAMLRequest"], BINDING_HTTP_REDIRECT
                )
            except KeyError:
                resp = BadRequest("Message signature verification failure")
                return resp(self.environ, self.start_response)

            if not self.req_info:
                resp = BadRequest("Message parsing failed")
                return resp(self.environ, self.start_response)

            _req = self.req_info.message

            if "SigAlg" in saml_msg and "Signature" in saml_msg:
                # Signed request
                issuer = _req.issuer.text
                _certs = IDP.metadata.certs(issuer, "any", "signing")
                verified_ok = False
                for cert in _certs:
                    if verify_redirect_signature(saml_msg, IDP.sec.sec_backend, cert):
                        verified_ok = True
                        break
                if not verified_ok:
                    resp = BadRequest("Message signature verification failure")
                    return resp(self.environ, self.start_response)

            if self.user:
                saml_msg["req_info"] = self.req_info
                if _req.force_authn is not None and _req.force_authn.lower() == "true":
                    key = self._store_request(saml_msg)
                    return self.not_authn(key, _req.requested_authn_context)
                else:
                    return self.operation(saml_msg, BINDING_HTTP_REDIRECT)
            else:
                saml_msg["req_info"] = self.req_info
                key = self._store_request(saml_msg)
                return self.not_authn(key, _req.requested_authn_context)
        else:
            return self.operation(saml_msg, BINDING_HTTP_REDIRECT)

    def post(self):
        """
        The HTTP-Post endpoint
        """
        logger.info("--- In SSO POST ---")
        saml_msg = self.unpack_either()

        try:
            _key = saml_msg["key"]
            saml_msg = IDP.ticket[_key]
            self.req_info = saml_msg["req_info"]
            del IDP.ticket[_key]
        except KeyError:
            self.req_info = IDP.parse_authn_request(
                saml_msg["SAMLRequest"], BINDING_HTTP_POST
            )
            _req = self.req_info.message
            if self.user:
                if _req.force_authn is not None and _req.force_authn.lower() == "true":
                    saml_msg["req_info"] = self.req_info
                    key = self._store_request(saml_msg)
                    return self.not_authn(key, _req.requested_authn_context)
                else:
                    return self.operation(saml_msg, BINDING_HTTP_POST)
            else:
                saml_msg["req_info"] = self.req_info
                key = self._store_request(saml_msg)
                return self.not_authn(key, _req.requested_authn_context)
        else:
            return self.operation(saml_msg, BINDING_HTTP_POST)

    # def artifact(self):
    # # Can be either by HTTP_Redirect or HTTP_POST
    #     _req = self._store_request(self.unpack_either())
    #     if isinstance(_req, basestring):
    #         return self.not_authn(_req)
    #     return self.artifact_operation(_req)


# -----------------------------------------------------------------------------
# === Authentication ====
# -----------------------------------------------------------------------------


def do_authentication(
    environ, start_response, authn_context, key, redirect_uri, headers=None
):
    """
    Display the login form
    """
    logger.debug("Do authentication")
    auth_info = AUTHN_BROKER.pick(authn_context)

    if len(auth_info):
        method, reference = auth_info[0]
        logger.debug("Authn chosen: %s (ref=%s)", method, reference)
        return method(environ, start_response, reference, key, redirect_uri, headers)
    else:
        resp = Unauthorized("No usable authentication method")
        return resp(environ, start_response)


# -----------------------------------------------------------------------------


def ng_passport_authn(
    environ, start_response, reference, key, redirect_uri, headers=None
):
    """
    Display the login form, or an error page if Newgrounds can't be reached
    """

    logger.info("do ng_passport_authn")

    kwargs = {"template_lookup": LOOKUP}
    if headers:
        kwargs["headers"] = headers

    try:
        # TODO Use an existing sessionID if possible
        r = requests.post(
            idp_conf.NGIO_URL,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            data={
                "input": json.dumps({
                    "app_id": idp_conf.APP_ID,
                    "call": {
                        "component": "App.startSession"
                    }
                })
            })
        r.raise_for_status()

        output = json.loads(r.content)
        session = output["result"]["data"]["session"]

        logger.info("The login page")
        kwargs["mako_template"] = "login.mako"

        argv = {
            "action": "/verify",
            "key": key,
            "authn_reference": reference,
            "redirect_uri": redirect_uri,

            "passport_url": session["passport_url"],
            "session_id": session["id"],
        }

        logger.info("do_authentication argv: %s", argv)
        resp = Response(**kwargs)

    except (KeyError, HTTPError):
        logger.info("The error page - bad response")
        kwargs["mako_template"] = "error.mako"
        argv = {
            "error_reason": "Invalid session",
            "error_action": "This is likely an internal error.<br>"
                            "<a href='mailto:webmaster@ngmvs.one'>Email the admin</a> to ask for this to be fixed!",
        }
        resp = BadGateway(**kwargs)

    except (ConnectionError, Timeout):
        logger.info("The error page - connection problem")
        kwargs["mako_template"] = "error.mako"
        argv = {
            "error_reason": "Unable to contact Newgrounds",
            "error_action": "Try logging in again later.",
        }
        resp = BadGateway(**kwargs)

    return resp(environ, start_response, **argv)


def do_check_session(environ, start_response, _):
    """
    Check if a session_id of a Newgrounds Passport session is for a logged-in user.
    SHOULD ONLY BE CALLED BY THE LOGIN PAGE.
    """
    query_str = get_post(environ)
    if not isinstance(query_str, six.string_types):
        query_str = query_str.decode("ascii")
    query = parse_qs(query_str)

    logger.debug("do_check_session: %s", query)

    try:
        session_id = query["session_id"][0]
        user = _do_check_session_internal(session_id)
        if user:
            resp = Response(message=json.dumps(user), content="application/json")
        else:
            resp = Unauthorized("Inactive NG Passport session")
    except (KeyError, HTTPError):
        resp = BadRequest("Unknown session")
    except (ConnectionError, Timeout):
        resp = BadRequest("Unable to contact Newgrounds")

    return resp(environ, start_response)


def _do_check_session_internal(session_id):
    r = requests.post(
        idp_conf.NGIO_URL,
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data={
            "input": json.dumps({
                "app_id": idp_conf.APP_ID,
                "session_id": session_id,
                "call": {
                    "component": "App.checkSession"
                }
            })
        })
    r.raise_for_status()

    output = json.loads(r.content)
    user = output["result"]["data"]["session"]["user"]
    return user


def do_verify(environ, start_response, _):
    query_str = get_post(environ)
    if not isinstance(query_str, six.string_types):
        query_str = query_str.decode("ascii")
    query = parse_qs(query_str)

    logger.debug("do_verify: %s", query)

    try:
        # TODO Store the session_id somewhere?
        session_id = query["sessionID"][0]
        try:
            udata = {
                "uid": query["id"][0],
                "displayName": query["name"][0],
                "member": "supporter" if query["supporter"][0] == "true" else "user",
                "jpegPhoto": query["icon"][0],
            }
        except KeyError:
            logger.info("Calling App.checkSession to get user info, since client either didn't pass it here or was missing some info")
            try:
                user = _do_check_session_internal(session_id)

                udata = {
                    "uid": str(user["id"]),
                    "displayName": user["name"],
                    "member": "supporter" if user["supporter"] else "user",
                    "jpegPhoto": user["icons"].get("large", "") if "icons" in user else "",
                }
            except (KeyError, HTTPError, TypeError):
                udata = None
                argv = {
                    "error_reason": "Invalid login session",
                    "error_action": "Make sure you're logged into Newgrounds Passport,<br>"
                                    "then refresh this page and try again.",
                }
            except (ConnectionError, Timeout):
                udata = None
                argv = {
                    "error_reason": "Unable to contact Newgrounds",
                    "error_action": "Try logging in again later.",
                }

    except KeyError:
        udata = None
        argv = {
            "error_reason": "Internal error",
            "error_action": "Did you get to this page from an invalid link?<br>"
                            "Press Back in your browser and try again.",
        }

    if not udata:
        logger.info("The error page")
        kwargs = dict(mako_template="error.mako", template_lookup=LOOKUP)
        resp = BadGateway(**kwargs)
    else:
        argv = {}
        uid = udata["uid"]
        IDP.cache.user2udata[uid] = udata
        logger.debug("Register %s under '%s'", udata["displayName"], uid)

        kaka = set_cookie("idpauthn", "/", uid, query["authn_reference"][0])

        lox = "%s?id=%s&key=%s" % (query["redirect_uri"][0], uid, query["key"][0])
        logger.debug("Redirect => %s", lox)
        resp = Redirect(lox, headers=[kaka], content="text/html")

    return resp(environ, start_response, **argv)


def not_found(environ, start_response):
    """Called if no URL matches."""
    resp = NotFound()
    return resp(environ, start_response)


# ----------------------------------------------------------------------------
# Cookie handling
# ----------------------------------------------------------------------------


def info_from_cookie(kaka):
    logger.debug("KAKA: %s", kaka)
    if kaka:
        cookie_obj = SimpleCookie(kaka)
        morsel = cookie_obj.get("idpauthn", None)
        if morsel:
            try:
                data = base64.b64decode(morsel.value)
                if not isinstance(data, six.string_types):
                    data = data.decode("ascii")
                key, ref = data.split(":", 1)
                return key, ref
            except (KeyError, TypeError):
                return None, None
        else:
            logger.debug("No idpauthn cookie")
    return None, None


def delete_cookie(environ, name):
    kaka = environ.get("HTTP_COOKIE", "")
    logger.debug("delete KAKA: %s", kaka)
    if kaka:
        cookie_obj = SimpleCookie(kaka)
        morsel = cookie_obj.get(name, None)
        cookie = SimpleCookie()
        cookie[name] = ""
        cookie[name]["path"] = "/"
        logger.debug("Expire: %s", morsel)
        cookie[name]["expires"] = _expiration("dawn")
        return tuple(cookie.output().split(": ", 1))
    return None


def set_cookie(name, _, *args):
    cookie = SimpleCookie()

    data = ":".join(args)
    if not isinstance(data, six.binary_type):
        data = data.encode("ascii")

    data64 = base64.b64encode(data)
    if not isinstance(data64, six.string_types):
        data64 = data64.decode("ascii")

    cookie[name] = data64
    cookie[name]["path"] = "/"
    cookie[name]["expires"] = _expiration(5)  # 5 minutes from now
    logger.debug("Cookie expires: %s", cookie[name]["expires"])
    return tuple(cookie.output().split(": ", 1))


# ----------------------------------------------------------------------------


# map urls to functions
AUTHN_URLS = [
    # sso
    (r"sso/post$", (SSO, "post")),
    (r"sso/post/(.*)$", (SSO, "post")),
    (r"sso/redirect$", (SSO, "redirect")),
    (r"sso/redirect/(.*)$", (SSO, "redirect")),
    (r"sso/art$", (SSO, "artifact")),
    (r"sso/art/(.*)$", (SSO, "artifact")),
]

NON_AUTHN_URLS = [
    (r"verify?(.*)$", do_verify),
    (r"check_session?(.*)$", do_check_session),
]


# ----------------------------------------------------------------------------


def metadata(environ, start_response):
    try:
        path = os.path.dirname(os.path.abspath(__file__))
        if path[-1] != "/":
            path += "/"
        metadata = create_metadata_string(
            idp_conf.CONFIG,
            IDP.config,
            args.valid,
            args.cert,
            args.keyfile,
            args.id,
            args.name,
            args.sign,
        )
        start_response("200 OK", [("Content-Type", "text/xml")])
        return [metadata]
    except Exception as ex:
        logger.error("An error occured while creating metadata: %s", ex)
        return not_found(environ, start_response)


def staticfile(environ, start_response):
    try:
        base_path = os.path.dirname(os.path.abspath(__file__))
        if base_path[-1] != "/":
            base_path += "/"
        path = base_path + environ.get("PATH_INFO", "").lstrip("/")
        path = os.path.realpath(path)
        if not path.startswith(base_path):
            resp = Unauthorized()
            return resp(environ, start_response)
        content_type = mimetypes.guess_type(path, True)[0]
        if not content_type:
            content_type = "text/xml"
        start_response("200 OK", [("Content-Type", content_type)])
        return open(path, "r").read()
    except Exception as ex:
        logger.error("An error occured while creating metadata: %s", ex)
        return not_found(environ, start_response)


def application(environ, start_response):
    """
    The main WSGI application. Dispatch the current request to
    the functions from above and store the regular expression
    captures in the WSGI environment as  `myapp.url_args` so that
    the functions from above can access the url placeholders.

    If nothing matches, call the `not_found` function.

    :param environ: The HTTP application environment
    :param start_response: The application to run when the handling of the
        request is done
    :return: The response as a list of lines
    """

    path = environ.get("PATH_INFO", "").lstrip("/")

    if path == "idp.xml":
        return metadata(environ, start_response)

    kaka = environ.get("HTTP_COOKIE", None)
    logger.info("<application> PATH: %s", path)

    if kaka:
        logger.info("= KAKA =")
        user, authn_ref = info_from_cookie(kaka)
        if authn_ref:
            environ["idp.authn"] = AUTHN_BROKER[authn_ref]
    else:
        try:
            query = parse_qs(environ["QUERY_STRING"])
            logger.debug("QUERY: %s", query)
            user = query["id"][0]
        except KeyError:
            user = None

    url_patterns = AUTHN_URLS
    if not user:
        logger.info("-- No USER --")
        # insert NON_AUTHN_URLS first in case there is no user
        url_patterns = NON_AUTHN_URLS + url_patterns

    for regex, callback in url_patterns:
        match = re.search(regex, path)
        if match is not None:
            try:
                environ["myapp.url_args"] = match.groups()[0]
            except IndexError:
                environ["myapp.url_args"] = path

            logger.debug("Callback: %s", callback)
            if isinstance(callback, tuple):
                cls = callback[0](environ, start_response, user)
                func = getattr(cls, callback[1])

                return func()
            return callback(environ, start_response, user)

    if re.search(r"static/.*", path) is not None:
        return [staticfile(environ, start_response).encode('utf-8')]
    return not_found(environ, start_response)


# ----------------------------------------------------------------------------


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v",
        dest="valid",
        help="How long, in days, the metadata is valid from " "the time of creation",
    )
    parser.add_argument("-c", dest="cert", help="certificate")
    parser.add_argument("-i", dest="id", help="The ID of the entities descriptor")
    parser.add_argument(
        "-k", dest="keyfile", help="A file with a key to sign the metadata with"
    )
    parser.add_argument("-n", dest="name")
    parser.add_argument(
        "-s", dest="sign", action="store_true", help="sign the metadata"
    )
    parser.add_argument("-m", dest="mako_root", default="./")
    args = parser.parse_args()

    AUTHN_BROKER = AuthnBroker()
    AUTHN_BROKER.add(
        authn_context_class_ref(TIMESYNCTOKEN), ng_passport_authn, 10, idp_conf.BASE
    )
    AUTHN_BROKER.add(authn_context_class_ref(UNSPECIFIED), "", 0, idp_conf.BASE)

    IDP = server.Server(idp_conf.CONFIG, cache=Cache())
    IDP.ticket = {}

    _rot = args.mako_root
    LOOKUP = TemplateLookup(
        directories=[_rot + "templates", _rot + "htdocs"],
        module_directory=_rot + "modules",
        input_encoding="utf-8",
        output_encoding="utf-8",
    )

    HOST = idp_conf.HOST
    PORT = idp_conf.PORT

    sign_alg = None
    digest_alg = None
    try:
        sign_alg = idp_conf.SIGN_ALG
    except AttributeError:
        pass
    try:
        digest_alg = idp_conf.DIGEST_ALG
    except AttributeError:
        pass
    ds.DefaultSignature(sign_alg, digest_alg)

    SRV = WSGIServer((HOST, PORT), application)

    _https = ""
    if idp_conf.HTTPS:
        https = "using HTTPS"
        # SRV.ssl_adapter = ssl_pyopenssl.pyOpenSSLAdapter(
        #     config.SERVER_CERT, config.SERVER_KEY, config.CERT_CHAIN)
        SRV.ssl_adapter = BuiltinSSLAdapter(
            idp_conf.SERVER_CERT, idp_conf.SERVER_KEY, idp_conf.CERT_CHAIN
        )

    logger.info("Server starting")
    print("IDP listening on %s:%s%s" % (HOST, PORT, _https))
    try:
        SRV.start()
    except KeyboardInterrupt:
        SRV.stop()
